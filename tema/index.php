<?php get_header(); ?>
    <main>
        <div class="Banner">
            <div class="Info-Container">
                <div class="Banner-Title"><h2><?php the_field('titulo_inicial_'); ?></h2></div>
                <div class="Banner-Line"></div>
                <div class="Banner-Subtitle"><h3><?php the_field('descricao_inicial'); ?></h3></div>
            </div>
        </div>
        <div class="Sobre">
            <div class="Sobre-Title"><h2><?php the_field('titulo_sobre'); ?></h2></div>
            <div class="Sobre-Subtitle"><?php the_field('descricao_sobre'); ?></div>
        </div>
        <div class="Valores">
            <div class="Valores-Title"><h3><?php the_field('valores_titulo_valores'); ?></h3></div>
            <div class="Valores-Boxes-Container">
                <div class="Protecao-Container">
                    <div class="Protecao-Icon-Container">
                        <div class="Protecao-Icon"></div>
                    </div>
                    <div class="Protecao-Title"><h3>
                        <?php the_field('valores_titulo_valor_1'); ?></h3>
                    </div>
                    <div class="Protecao-Descricao">
                        <?php the_field('valores_descricao_valor_1'); ?>
                    </div>
                </div>
                <div class="Carinho-Container">
                    <div class="Carinho-Icon-Container">
                        <div class="Carinho-Icon"></div>
                    </div>
                    <div class="Carinho-Title"><h3>
                        <?php the_field('valores_titulo_valor_2'); ?>
                    </h3></div>
                    <div class="Carinho-Descricao">
                        <?php the_field('valores_descricao_valor_2'); ?>
                    </div>
                </div>
                <div class="Companheirismo-Container">
                    <div class="Companheirismo-Icon-Container">
                        <div class="Companheirismo-Icon"></div>
                    </div>
                    <div class="Companheirismo-Title"><h3>
                        <?php the_field('valores_titulo_valor_3'); ?>
                    </h3></div>
                    <div class="Companheirismo-Descricao">
                        <?php the_field('valores_descricao_valor_3'); ?>
                    </div>
                </div>
                <div class="Resgate-Container">
                    <div class="Resgate-Icon-Container">
                        <div class="Resgate-Icon"></div>
                    </div>
                    <div class="Resgate-Title"><h3>
                        <?php the_field('valores_titulo_valor_4'); ?>
                    </h3></div>
                    <div class="Resgate-Descricao">
                        <?php the_field('valores_descricao_valor_4'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="Lobo-Exemplo">
            <div class="Lobo-Title"><h3><?php the_field('titulo_exemplo'); ?></h3></div>

            <?php 
                $the_query = new WP_Query(array('posts_per_page' => 2));
            ?>
            <?php $numero = 0 ?>

            <?php 
                while($the_query -> have_posts()) : $the_query -> the_post();
            ?>
             <?php 
                if($numero % 2 == 0){ ?>

            <div class="Lobo-Box" id="wrap">
                <div class="Lobo-Foto-Container">
                    <div class="Lobo-DuplaFoto-Container">
                        <div class="Foto-Behind"></div>
                        <figure class="Lobo-Foto">
                            <?php if( get_field('post_lobo_imagem') ): ?>
                                <img src="<?php the_field('post_lobo_imagem'); ?>" />
                            <?php endif; ?>
                        </figure>
                    </div>
                </div>
                <div class="Lobo-Info-Container">
                    <div class="Lobo-Name"><?php the_field('post_lobo_nome_lobo'); ?></div>
                    <div class="Lobo-Idade">Idade: <?php the_field('post_lobo_idade_lobo'); ?> anos</div>
                    <div class="Lobo-Texto-Container">
                        <div class="Lobo-Texto">
                            <?php the_field('post_lobo_descricao_lobo'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php }
            else{ ?>

            <div class="Lobo-Box" id="reverse">
                <div class="Lobo-Info-Container">
                    <div class="Lobo-Name"><?php the_field('post_lobo_nome_lobo'); ?></div>
                    <div class="Lobo-Idade">Idade: <?php the_field('post_lobo_idade_lobo'); ?> anos</div>
                    <div class="Lobo-Texto-Container">
                        <div class="Lobo-Texto">
                            <?php the_field('post_lobo_descricao_lobo'); ?>
                        </div>
                    </div>
                </div>
                <div class="Lobo-Foto-Container">
                    <div class="Lobo-DuplaFoto-Container">
                        <div class="Foto-Behind"></div>
                        <figure class="Lobo-Foto">
                            <?php if( get_field('post_lobo_imagem') ): ?>
                                <img src="<?php the_field('post_lobo_imagem'); ?>" />
                            <?php endif; ?>
                        </figure>
                    </div>
                </div>
            </div>
            <?php }
                $numero++; ?>
            <?php 
                endwhile;
                wp_reset_postdata();
            ?>       
        </div>
        

    </main>
    <?php get_footer(); ?>