<footer>
        <div class="degrade"></div>
        <div class="conteiner1"> 
            <div class="mapa">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14700.7537673028!2d-43.1332584!3d-22.9064193!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfd5e35fb577af2f5!2sUFF%20-%20Instituto%20de%20Computa%C3%A7%C3%A3o!5e0!3m2!1spt-BR!2sbr!4v1659146720082!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
        <div class="conteiner2">
            <div class="location">
                <p class="icons"><img src="/lobinho/wp-content/themes/tema/resources/Vector.png" class="icone_location"></img>Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa<br> Viagem, Niterói - RJ, 24210-315</p>
            </div>
            <div class="contato">
                <p class="icons"><i class="material-icons custom">phone</i>(99) 99999-9999</p>
            </div>
            <div class="email">
                <p class="icons"><i class="material-icons custom">email</i>salve-lobos@lobINhos.com</p>
            </div>
            <div class="quem_somos">
                <p><a href="http://localhost/lobinho/quem-somos/">Quem Somos</a></p>
            </div>
        </div>
        <div class="conteiner3">
            <div class="dev">
                <p>Desenvolvida com</p>
            </div>
            <div class="imagem_pata">
                <img src="/lobinho/wp-content/themes/tema/resources/pata.png">
            </div>
        </div>
    </footer>
    <script src="js/PromiseHandler.js"></script>
    <script src="js/GetLobosHandler.js"></script>
    <?php wp_footer(); ?>
</body>
</html>